 RITI DI INTRODUZIONE

Sac: Nel nome del Padre e del Figlio e dello Spirito Santo

Tutti: Amen

Sac: Il Signore che guida i nostri cuori nell’amore e nella pazienza di Cristo sia con tutti voi

Tutti: E con il tuo spirito

MEMORIA DEL BATTESIMO

Sac: Carissimi, celebriamo il grande mistero dell’amore di Cristo per la sua Chiesa. Oggi @@ e ## sono chiamati a parteciparvi con il loro Matrimonio. Riconoscenti per essere divenuti figli nel Figlio, facciamo ora memoria del Battesimo, inizio della nuova vita nella fede, sorgente e fondamento di ogni vocazione. Dio nostro Padre, con la forza del suo Santo Spirito, ravvivi in tutti noi il dono di quella benedizione originaria.

Sac: Padre, nel Battesimo del tuo Figlio Gesù al fiume Giordano hai rivelato al mondo l’amore sponsale per il tuo popolo.

Tutti: Noi ti lodiamo e ti rendiamo grazie.

Sac: Cristo Gesù, dal tuo costato aperto sulla Croce hai generato la Chiesa, tua diletta sposa.

Tutti: Noi ti lodiamo e ti rendiamo grazie.

Sac: Spirito Santo, potenza del Padre e del Figlio, oggi fai risplendere in @@ e ## la veste nuziale della Chiesa.

Tutti: Noi ti lodiamo e ti rendiamo grazie.

Sac: Dio onnipotente, origine e fonte della vita, che ci hai rigenerati nell’acqua con la potenza del tuo Spirito, ravviva in noi la grazia del Battesimo, e concedi a @@ e ## un cuore libero e una fede ardente perché, purificati nell’intimo, accolgano il dono del Matrimonio, nuova via della loro santificazione. Per Cristo nostro Signore.

Tutti: Amen

GLORIA A DIO

Tutti: Gloria a Dio nell’alto dei cieli e pace in terra agli uomini di buona volontà. Noi ti lodiamo, ti benediciamo, ti adoriamo, ti glorifichiamo, ti rendiamo grazie per la tua gloria immensa, Signore Dio, re del cielo, Dio Padre onnipotente. Signore, Figlio unigenito, Gesù Cristo, Signore Dio, Agnello di Dio, Figlio del Padre, tu che togli i peccati del mondo, abbi pietà di noi; tu che togli i peccati del mondo accogli la nostra supplica; tu che siedi alla destra del Padre, abbi pietà di noi. Perché tu solo il santo, tu solo il Signore, tu solo l’Altissimo, Gesù Cristo, con lo Spirito Santo nella gloria di Dio Padre. Amen.

ALL’INIZIO DELL’ASSEMBLEA LITURGICA

Sac: Preghiamo

O Dio onnipotente, guarda benigno @@ e ##, che oggi consacrano il loro amore, e fa' che crescano insieme nella fede che professano davanti a te e allietino con i loro figli la tua santa Chiesa. Per Gesù Cristo, tuo Figlio, nostro Signore e nostro Dio, che vive e regna con te, nell'unità dello Spirito santo, per tutti i secoli dei secoli.

Tutti: Amen.

LITURGIA DELLA PAROLA

LETTURA

@prima-lettura@

@seconda-lettura@

CANTO AL VANGELO

@vangelo@

OMELIA DEL SACERDOTE

RITO DEL MATRIMONIO

Sac: Carissimi @@ e ## Siete venuti nella casa del Signore, davanti al ministro della Chiesa e davanti alla comunità, perché la vostra decisione di unirvi in matrimonio riceva il sigillo dello Spirito Santo, sorgente dell’amore fedele e inesauribile. Ora Cristo vi rende partecipi dello stesso amore con cui egli ha amato la sua Chiesa, fino a dare se stesso per lei.

Vi chiedo pertanto di esprimere le vostre intenzioni.

Sposi: Compiuto il cammino del fidanzamento, illuminati dallo Spirito Santo e accompagnati dalla comunità cristiana, siamo venuti in piena libertà nella casa del Padre perché il nostro amore riceva il sigillo di consacrazione. Consapevoli della nostra decisione, siamo disposti, con la grazia di Dio, ad amarci e sostenerci l’un l’altro per tutti i giorni della vita. [Ci impegniamo ad accogliere con amore i figli che Dio vorrà donarci e a educarli secondo la Parola di Cristo e l’insegnamento della Chiesa].

Chiediamo a voi fratelli e sorelle, di pregare con noi e per noi perché la nostra famiglia diffonda nel mondo luce, pace e gioia.

MANIFESTAZIONE DEL CONSENSO

Sac: Alla presenza di Dio e davanti alla Chiesa qui riunita, datevi la mano destra ed esprimete il vostro consenso. Il Signore, inizio e compimento del vostro amore sia con voi per sempre.

Sac: @@, vuoi accogliere ## come tua sposa nel Signore, promettendo di esserle fedele sempre. Nella gioia e nel dolore, nella salute e nella malattia, e di amarla e di onorarla tutti i giorni della tua vita?

Sposo: Sì

Sac: ##, vuoi accogliere @@ come tuo sposo nel Signore, promettendo di essergli fedele sempre, nella gioia e nel dolore, nella salute e nella malattia, e di amarlo e di onorarlo tutti i giorni della tua vita?

Sposa : Sì

Sac: Il Dio di Abramo, il Dio di Isacco, il Dio di Giacobbe, il Dio che nel paradiso ha unito Adamo ed Eva confermi in Cristo il consenso che avete manifestato davanti alla Chiesa e vi sostenga con la sua benedizione. L’uomo non osi separare ciò che Dio unisce.

Tutti: Amen

BENEDIZIONE E CONSEGNA DEGLI ANELLI

Sac: Signore, benedici (+) e santifica l’amore di questi sposi: l’anello che porteranno come simbolo di fedeltà li richiami continuamente al vicendevole amore. Per Cristo nostro Signore.

Tutti: Amen

Sposo: @@, ricevi questo anello, segno del mio amore e della mia fedeltà. Nel nome del Padre, del Figlio e dello Spirito Santo.

Sposa: ##, ricevi questo anello, segno del mio amore e della mia fedeltà. Nel nome del Padre, del Figlio e dello Spirito Santo.

BENEDIZIONE DEGLI SPOSI

Sac: Fratelli e sorelle, raccolti in preghiera, invochiamo su questi sposi, @@ e ##, la benedizione di Dio: egli che oggi li ricolma di grazia con il sacramento del matrimonio, li accompagni sempre con la sua protezione.

Sac: Padre santo, creatore dell’universo, che hai formato l’uomo e la donna a tua immagine, e hai voluto benedire la loro unione, ti preghiamo umilmente per questi tuoi figli con il sacramento nuziale.

[Sac: Ti lodiamo, Signore, e ti benediciamo

Tutti: Eterno è il tuo amore per noi]

Scenda, o Signore, su questi sposi @@ e ## la ricchezza delle tue benedizioni e la forza del tuo santo Spirito infiammi dall’alto i loro cuori, perché nel dono reciproco dell’amore, allietino di figli la loro famiglia e la comunità ecclesiale.

[Sac: Ti lodiamo, Signore, e ti benediciamo

Tutti: Eterno è il tuo amore per noi]

Ti lodino, Signore, nella gioia, ti cerchino nella sofferenza; godano del tuo sostegno nella fatica e del tuo conforto nella necessità; ti preghino nella santa assemblea, siano tuoi testimoni nel mondo.

Vivano a lungo nella prosperità e nella pace, e con tutti gli amici che ora li circondano, giungano alla felicità del tuo regno. Per Cristo nostro Signore

Tutti: Amen

Sac: Benediciamo il Signore

Tutti: A lui onore e gloria nei secoli

INVOCAZIONE DEI SANTI

Sac: Fratelli e sorelle, consapevoli del singolare dono di grazia e carità, per mezzo del quale Dio ha voluto rendere perfetto e consacrare l’amore dei nostri fratelli @@ e ##, chiediamo al Signore che, sostenuti dall’esempio e dall’intercessione dei santi, essi custodiscano nella fedeltà il loro vincolo coniugale.

Santa Maria, Madre di Dio, prega per noi
Santa Maria, Madre della Chiesa, prega per noi
Santa Maria, Regina della Famiglia, prega per noi
San Giuseppe, Sposo di Maria, prega per noi
Santi Angeli di Dio, pregate per noi
Santi Gioacchino e Anna, pregate per noi
Santi Zaccaria e Elisabetta, pregate per noi
San Giovanni Battista, prega per noi
Santi Pietro e Paolo, pregate per noi
Santi Apostoli ed Evangelisti, pregate per noi
Santi Martiri di Cristo, pregate per noi
Santi Aquila e Priscilla, pregate per noi
Santi Mario e Marta, pregate per noi
Santa Monica, prega per noi
San Paolino, prega per noi
Santa Brigida, prega per noi
Santa Rita, prega per noi
Santa Francesca Romana, prega per noi
San Tommaso Moro, prega per noi
Santa Giovanna Beretta Molla prega per noi
San [nome dello sposo] prega per noi
Santa [nome della sposa] prega per noi
San (patrono parrocchia) prega per noi
Santi e Sante tutte pregate per noi

Tutti: Ascoltaci, Signore

Lett: Concedi che @@ e ## attraverso l’unione santa del matrimonio, possano godere della salute del corpo e della salvezza eterna.

Tutti: Ascoltaci, Signore

Lett: Benedici l’unione di questi sposi come santificasti le nozze di Cana.

Tutti: Ascoltaci, Signore

Lett: Rendi fecondo l’amore di @@ e ## e concedi loro pace e sostegno, perchésiano testimoni fedeli di vita cristiana.

Tutti: Ascoltaci, Signore

Lett: Dona al popolo cristiano di crescere di giorno in giorno nella certezza della fede.

Tutti: Ascoltaci, Signore

Lett: Concedi l’aiuto della tua grazia a tutti coloro che sono oppressi dalle difficoltà della vita.

Tutti: Ascoltaci, Signore

Lett: Effondi lo Spirito Santo perché rinnovi in tutti gli sposi qui presenti la grazia del sacramento.

Tutti: Ascoltaci, Signore

Kyrie elèison. Kyrie elèison.
Kyrie elèison. Kyrie elèison.
Kyrie elèison. Kyrie elèison.

A CONCLUSIONE DELLA LITURGIA DELLA PAROLA

Sac: Dio onnipotente, volgiti propizio a questi tuoi figli che a te affidano la loro speranza; concedi loro i doni della tua grazia perché conservino l’amore nella loro unione e al termine di questa vita terrena entrino nella beatitudine eterna. Per Cristo nostro Signore.

Tutti: Amen

LITURGIA EUCARISTICA

Sac: Secondo l'ammonimento del Signore, prima di presentare i nostri doni all'altare, scambiamoci un segno di pace

PRESENTAZIONE DEI DONI

Sac: Benedetto sei tu, Signore, Dio dell’universo: dalla tua bontà abbiamo ricevuto questo pane, frutto della terra e del lavoro dell’uomo; lo presentiamo a te perché diventi per noi cibo di vita eterna.

Tutti: Benedetto nei secoli il Signore

Sac: Benedetto sei tu, Signore, Dio dell’universo: dalla tua bontà abbiamo ricevuto questo vino, frutto della vite e del lavoro dell’uomo; lo presentiamo a te perché diventi per noi bevanda di salvezza.

Tutti: Benedetto nei secoli il Signore

ORAZIONE SUI DONI

Sac: Accogli, o Dio, i doni e le preghiere che ti presentiamo per @@ e ##, uniti nel vincolo santo; questo mistero, che esprime la pienezza della tua carità, custodisca sempre il loro amore sponsale. Per Cristo nostro Signore.

Tutti: Amen

PREGHIERA EUCARISTICA

Sac: Il Signore sia con voi

Tutti: E con il tuo spirito

Sac: In alto i nostri cuori

Tutti: Sono rivolti al Signore

Sac: Rendiamo grazie al Signore, nostro Dio

Tutti: E’ cosa buona e giusta

Sac: È veramente cosa buona e giusta, nostro dovere e fonte di salvezza, rendere grazie sempre, qui e in ogni luogo, a te, Signore, Padre santo, Dio onnipotente ed eterno. Tu hai dato all’uomo il dono dell’esistenza e lo hai innalzato a una dignità incomparabile; nella comunione tra l’uomo e la donna hai impresso un’immagine del tuo amore. Così la tua immensa bontà, che in principio ha creato la famiglia, incessantemente la sospinge verso la gioia di una comunione perenne. E in questo disegno stupendo il sacramento che in Cristo consacra l’amore umano ci offre un segno e una primizia della tua carità. E noi, ammirati e riconoscenti, insieme con gli angeli e coi santi, cantiamo l’inno della tua gloria:

Tutti: Santo, Santo, Santo il Signore Dio dell’universo. I cieli e la terra sono pieni della tua gloria. Osanna nell’alto dei cieli. Benedetto colui che viene nel nome del Signore. Osanna nell’alto dei cieli

Sac: Veramente Santo sei tu o Padre e fonte di ogni santità, santifica questi doni con l’effusione del tuo Spirito perché diventino per noi il corpo e (+) il sangue di Gesù Cristo nostro Signore.

Egli, offrendosi liberamente alla sua passione, prese il pane e rese grazie, lo spezzò, lo diede ai suoi discepoli, e disse:

PRENDETE, E MANGIATENE TUTTI:

QUESTO É IL MIO CORPO

OFFERTO IN SACRIFICIO PER VOI.

Dopo la cena, allo stesso modo, prese il calice e rese grazie, lo diede ai suoi discepoli, e disse:

PRENDETE, E BEVETENE TUTTI:

QUESTO É IL CALICE DEL MIO SANGUE

PER LA NUOVA ED ETERNA ALLEANZA,

VERSATO PER VOI E PER TUTTI

IN REMISSIONE DEI PECCATI.

FATE QUESTO IN MEMORIA DI ME.

Mistero della fede.

Tutti: Annunciamo la tua morte, Signore, proclamiamo la tua risurrezione, nell’attesa della tua venuta.

Celebrando il memoriale della morte e risurrezione del tuo Figlio, ti offriamo, Padre, il pane della vita e il calice della salvezza, e ti rendiamo grazie per averci ammessi alla tua presenza a compiere il servizio sacerdotale. Ti preghiamo umilmente: per la comunione al corpo e al sangue di Cristo lo Spirito Santo ci riunisca in un solo corpo.

Ricordati, Padre, della tua Chiesa diffusa su tutta la terra: rendila perfetta nell’amore in unione con il nostro Papa Francesco, il nostro Vescovo Mario, e tutto l’ordine sacerdotale.

Ricordati dei tuoi figli @@ e ##, che in Cristo hanno costituito una nuova famiglia, piccola Chiesa e sacramento del tuo amore, perché la grazia di questo giorno si estenda a tutta la loro vita.

Ricordati dei nostri fratelli, che si sono addormentati nella speranza della risurrezione e di tutti i defunti che si affidano alla tua clemenza: ammettili a godere la luce del tuo volto.

Di noi tutti abbi misericordia: donaci di aver parte alla vita eterna, insieme con la beata Maria, Vergine e Madre di Dio, San Giuseppe suo sposo, Sant'Ambrogio, gli apostoli e tutti i santi, che in ogni tempo ti furono graditi: e in Gesù Cristo tuo Figlio canteremo la tua gloria.

Per Cristo, con Cristo e in Cristo, a te, Dio, Padre onnipotente, nell’unita dello Spirito Santo, ogni onore e gloria, per tutti i secoli dei secoli.

Tutti: Amen.

RITI DI COMUNIONE

Sac: Obbedienti alla parola del Salvatore e formati al suo divino insegnamento, osiamo dire:

Tutti: Padre nostro, che sei nei cieli, sia santificato il tuo nome, venga il tuo regno, sia fatta la tua volontà, come in cielo così in terra. Dacci oggi il nostro pane quotidiano, e rimetti a noi i nostri debiti come noi li rimettiamo ai nostri debitori, e non ci indurre in tentazione, ma liberaci dal male.

Sac: Liberaci, o Signore, da tutti i mali, concedi la pace ai nostri giorni; e con l’aiuto della tua misericordia, vivremo sempre liberi dal peccato e sicuri da ogni turbamento, nell’attesa che si compia la beata speranza, e venga il nostro Salvatore Gesù Cristo.

Tutti: Tuo é il regno, tua la potenza e la gloria nei secoli.

Sac: Signore Gesù Cristo, che hai detto ai tuoi apostoli: “Vi lascio la pace, vi do la mia pace”, non guardare ai nostri peccati, ma alla fede della tua Chiesa, e donale unità e pace secondo la tua volontà. Tu che vivi e regni nei secoli dei secoli.

Tutti: Amen.

Sac: La pace e la comunione del Signore nostro Gesù Cristo siano sempre con voi.

Tutti: E con il tuo spirito

Sac: Beati gli invitati alla cena del Signore. Ecco l’Agnello di Dio che toglie i peccati del mondo.

Tutti: O Signore, non son degno di partecipare alla tua mensa: ma dì soltanto una parola e io sarò salvato.

ORAZIONE DOPO LA COMUNIONE

Sac: Preghiamo

O Dio, sorgente di ogni salvezza, la grazia del sacramento nuziale operi di giorno in giorno nella vita di questi sposi, e il sacrificio che abbiamo offerto ci edifichi tutti nella carità. Per Cristo nostro Signore.

Tutti: Amen

LETTURA DEGLI ARTICOLI DEL CODICE CIVILE

Sac: Carissimi @@ e ##, avete celebrato il sacramento del Matrimonio manifestando il vostro consenso dinanzi a me ed ai testimoni. Oltre la grazia divina e gli effetti stabiliti dai sacri Canoni, il vostro Matrimonio produce anche gli effetti civili secondo le leggi dello Stato.

Vi do quindi lettura degli articoli del Codice civile riguardanti i diritti e i doveri dei coniugi che voi siete tenuti a rispettare ed osservare:

Art. 143: Con il matrimonio il marito e la moglie acquistano gli stessi diritti e assumono i medesimi doveri. Dal matrimonio deriva l’obbligo reciproco alla fedeltà, all’assistenza morale e materiale, alla collaborazione nell’interesse della famiglia e alla coabitazione.

Entrambi i coniugi sono tenuti, ciascuno in relazione alle proprie sostanze e alla propria capacità di lavoro professionale o casalingo, a contribuire ai bisogni della famiglia.

Art. 144: I coniugi concordano tra loro l’indirizzo della vita familiare e fissano la residenza della famiglia secondo le esigenze di entrambi e quelle preminenti della famiglia stessa. A ciascuno dei coniugi spetta il potere di attuare l’indirizzo concordato.

Art. 147: «Il matrimonio impone ad ambedue i coniugi l’obbligo di mantenere, istruire, educare e assistere moralmente i figli, nel rispetto delle loro capacità, inclinazioni naturali e aspirazioni, secondo quanto previsto dall’articolo 315-bis».

L’art. 315-bis del codice civile (Diritti e doveri del figlio) così dispone:

«Il figlio ha diritto di essere mantenuto, educato, istruito e assistito moralmente dai genitori, nel rispetto delle sue capacità, delle sue inclinazioni naturali e delle sue aspirazioni. Il figlio ha diritto di crescere in famiglia e di mantenere rapporti significativi con i parenti. Il figlio minore che abbia compiuto gli anni dodici, e anche di età inferiore ove capace di discernimento, ha diritto di essere ascoltato in tutte le questioni e le procedure che lo riguardano. Il figlio deve rispettare i genitori e deve contribuire, in relazione alle proprie capacità, alle proprie sostanze e al proprio reddito, al mantenimento della famiglia finché convive con essa».

RITI DI CONCLUSIONE

Sac: Il Signore sia con voi

Tutti: E con il tuo spirito.

Kyrie Eleison, Kyrie Eleison, Kyrie Eleison

Sac: Il Signore Gesù, che santificò le nozze di Cana, benedica voi, i vostri parenti e amici.

Tutti: Amen

Sac: Cristo, che ha amato la sua Chiesa sino alla morte, effonda continuamente nei vostri cuori il suo stesso amore.

Tutti: Amen

Sac: Il Signore conceda a voi, che testimoniate la fede nella sua risurrezione, di attendere nella gioia che si compia la beata speranza.

Tutti: Amen

Sac: E su voi tutti che avete partecipato a questa liturgia nuziale, scenda la benedizione di Dio onnipotente, Padre e Figlio (+) e Spirito Santo.

Tutti: Amen

Sac: Andiamo in pace

Tutti: Nel nome di Cristo 
