# Wedding Booklet

Questa repo contiene il sorgente LaTeX che ho utilizzato per costruire il mio libretto di matrimonio.

## Utilizzo
Nel file `Main.tex` è contenuto il testo completo del libretto.

Nel file `Setting.tex` sono contenuti varie configurazioni relative ai font, alle intestazioni e ai piè di pagine, ecc.

Nel file `Copertina.tex` è definita la copertina.

La cartella `schemas` contiene tre "modelli" utilizzabili per il testo del libretto. Tutti e tre gli schemi derivano dal cerimoniale ambrosiano.

La cartella `pictures` contiene eventuali immagini da utilizzare nel libretto.

La cartella `canzoni` contiene il testo delle canzoni.

La cartella `emerald` contiene il sorgente del pacchetto emerald, utilizzato per i font calligrafici.

## Compilazione
Il progetto prevede una pipeline per la compilazione automatica del libretto. 

La pipeline non è al momento funzionante: non è possibile installare correttamente il pacchetto `emerald` per i font calligrafici.
